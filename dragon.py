#!/usr/bin/python3
import RPi.GPIO as GPIO
import time
from threading import Thread, Lock
import threading
from pygame import mixer 
import random
from datetime import datetime
import os
import http.server
import socketserver
from urllib.parse import urlparse
from urllib.parse import parse_qs
from urllib.parse import parse_qsl
import signal
import json

GPIO.setmode(GPIO.BOARD) #Definit le mode de numerotation (Board)

PIN_MOTION_PIERRE = 7
PIN_MOTION_FRONT = 10
PIN_LED_PIERRE = 8 #Definit le numero du port GPIO qui alimente la led
PIN_BTN = 21
PIN_LED_BOUCHE_1 = 16
PIN_LED_BOUCHE_2 = 18
PIN_LED_BOUCHE_3 = 22

TIME_FIRST_CRI = 2
TIME_OTHER_CRI = 4

GPIO.setup(PIN_MOTION_PIERRE, GPIO.IN)
GPIO.setup(PIN_MOTION_FRONT, GPIO.IN)
GPIO.setup(PIN_LED_PIERRE, GPIO.OUT) #Active le controle du GPIO
GPIO.setup(PIN_LED_BOUCHE_1, GPIO.OUT) #Active le controle du GPIO
GPIO.setup(PIN_LED_BOUCHE_2, GPIO.OUT) #Active le controle du GPIO
GPIO.setup(PIN_LED_BOUCHE_3, GPIO.OUT) #Active le controle du GPIO
GPIO.setup(PIN_BTN, GPIO.IN)

nb = 0

isMotionActive = True
ENABLE_SOUND = True

hasMotionPierre = False
hasMotionFront = False
forcePierreUntil = 0  # permet de forcer le cycle de la pierre pendant un certains temps


mixer.init(44100,-16,1,1024)

sndDragon1 = mixer.Sound('sounds/3301.wav')
sndDragon2 = mixer.Sound('sounds/3302.wav')
sndDragon3 = mixer.Sound('sounds/17020.wav')

pwm_led_status = None # si on veut, il faut la recopier de dinopi

def switchStatus():
    global isMotionActive, pwm_led_status
    if isMotionActive:
        disableStatus()
    else:
        enableStatus()

def disableStatus():
    global isMotionActive, pwm_led_status
    print("disable Motion")
    isMotionActive = False
    # pwm_led_status.ChangeDutyCycle(30)
    # pwm_led_status.ChangeFrequency(1)

def enableStatus():
    global isMotionActive, pwm_led_status
    print("enable Motion")
    # pwm_led_status.ChangeDutyCycle(10)
    # pwm_led_status.ChangeFrequency(100)
    isMotionActive = True

# make sure that PIN_LED_PIERRE is off
GPIO.output(PIN_LED_PIERRE, GPIO.LOW)
GPIO.output(PIN_LED_BOUCHE_1, GPIO.LOW)
GPIO.output(PIN_LED_BOUCHE_2, GPIO.LOW)
GPIO.output(PIN_LED_BOUCHE_3, GPIO.LOW)

tpush = None
def PUSHBUTTON(PIN):
    global tpush
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    if not GPIO.input(PIN) :
        print("push detected")
        tpush = timestamp
    else:
        print("release detected")
        if tpush != None and ((timestamp - tpush) < 2):
            print("poweroff")
            os.system('/usr/sbin/poweroff')


def MOTION_PIERRE(PIN):
  global nb, hasMotionPierre, mixer, ENABLE_SOUND
  nb = nb + 1
  if GPIO.input(PIN) :
    print("Motion Pierre Detected!" + str(nb))
    #GPIO.output(PIN_LED_PIERRE, GPIO.HIGH) #On l'allume
    hasMotionPierre = True
    cyclePierre()
  else :
    print("no more Motion Pierre!" + str(nb))
    hasMotionPierre = False
    # s'il n'y a pas de son en court a ce moment, on fait un petit grogement
    if not mixer.get_busy() and ENABLE_SOUND:
        sndDragon3.play()

def MOTION_FRONT(PIN):
  global hasMotionFront
  if GPIO.input(PIN) :
    print("Motion Front Detected!")
    #GPIO.output(PIN_LED_PIERRE, GPIO.HIGH) #On l'allume
    hasMotionFront = True
    cycleFront()
  else :
    print("no more Motion Front!")
    hasMotionFront = False

threadPierre = None
threadFront = None

tCriDragon = None
def cycleSound():
    global tCriDragon, mixer
    if mixer.get_busy() or not ENABLE_SOUND:
        return
    # cri seulement apres 10s
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    if tCriDragon == None:
        tCriDragon = timestamp+TIME_FIRST_CRI
    elif timestamp - tCriDragon > TIME_OTHER_CRI * 2:
        # dans ce cas, on reinitialise le systeme, il n'y a plus de mouvement a priori
        tCriDragon = timestamp+TIME_FIRST_CRI
    elif timestamp - tCriDragon > 0:
        random.seed()
        s = random.choice([sndDragon1, sndDragon2])
        s.play()
        tCriDragon = timestamp+TIME_OTHER_CRI

def cycleFront():
    global threadFront
    if threadFront:
        threadFront.join(0.1)
        if threadFront.is_alive():
            return
    threadFront = CycleFront()
    threadFront.start()

def cyclePierre():
    global threadPierre
    if threadPierre:
        threadPierre.join(0.1)
        if threadPierre.is_alive():
            return
    threadPierre = CyclePierre()
    threadPierre.start()

class CyclePierre (Thread):
    """ Thread executant le script php, puis deplacant le ticket dans done """

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global hasMotionPierre, forcePierreUntil
        p = GPIO.PWM(PIN_LED_PIERRE, 80)
        #p.ChangeFrequency(10)
        p.start(0)
        try:
            now = datetime.now()
            timestamp = datetime.timestamp(now)
            isPierreForce = timestamp < forcePierreUntil
            print("Start CyclePierre")
            if isPierreForce:
                print(" - with isPierreForce")
            else:
                print(str(forcePierreUntil)+" < "+str(timestamp))
            if hasMotionFront:
                print(" - with hasMotionPierre")
            while hasMotionPierre or isPierreForce:
                for i in range(0, 4, 1):
                    cycleSound()
                    p.ChangeFrequency(80)
                    for j in range(0,100, 4):
                        p.ChangeDutyCycle(j)
                        time.sleep(0.03)
                    cycleSound()
                    for j in range(100, 0, -4):
                        p.ChangeDutyCycle(j)
                        time.sleep(0.03)
                    now = datetime.now()
                    timestamp = datetime.timestamp(now)
                    isPierreForce = timestamp < forcePierreUntil
                    if not hasMotionPierre and not isPierreForce:
                        raise Exception()
                p.ChangeFrequency(20)
                p.ChangeDutyCycle(10)
                time.sleep(2)
        except Exception: 
            pass
        finally:
            p.stop()
            GPIO.output(PIN_LED_PIERRE, GPIO.LOW) #On l'eteint 

class CycleFront (Thread):
    """ Thread executant le script php, puis deplacant le ticket dans done """

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global hasMotionFront, ENABLE_SOUND, sndDragon3
        print("start cycle front")
        p1 = GPIO.PWM(PIN_LED_BOUCHE_1, 60)
        p2 = GPIO.PWM(PIN_LED_BOUCHE_2, 80)
        p3 = GPIO.PWM(PIN_LED_BOUCHE_3, 100)
        p1.start(0)
        p2.start(0)
        p3.start(0)
        now = datetime.now()
        tstart = datetime.timestamp(now)
        initstep = True
        try:
            if not mixer.get_busy() and ENABLE_SOUND:
                sndDragon3.play()
            while hasMotionFront or initstep:
                # vague initial
                for j in range(0,166, 4):
                    p1.ChangeDutyCycle(min(j,100))
                    if j > 33:
                        p2.ChangeDutyCycle(min(j-33,100))
                    if j > 66:
                        p3.ChangeDutyCycle(min(j-66,100))
                    time.sleep(0.03)

                # incendie
                for i in range(0,30,1):
                    val = random.randint(5, 100)
                    p1.ChangeDutyCycle(val)
                    val = random.randint(5, 100)
                    p2.ChangeDutyCycle(val)
                    val = random.randint(5, 100)
                    p3.ChangeDutyCycle(val)
                    time.sleep(0.2)
                    now = datetime.now()
                    tnow = datetime.timestamp(now)

                # vague final
                for j in range(166, 0, -4):
                    p1.ChangeDutyCycle(min(100, max(j-66,0)))
                    if j > 33:
                        p2.ChangeDutyCycle(min(100,max(j-33,0)))
                    if j > 66:
                        p3.ChangeDutyCycle(min(100,max(j,0)))
                    time.sleep(0.03)

                if tnow - tstart > 5:
                    initstep = False

#        except Exception: 
#            pass
        finally:
            p1.stop()
            p2.stop()
            p3.stop()
            GPIO.output(PIN_LED_BOUCHE_1, GPIO.LOW) #On l'eteint 
            GPIO.output(PIN_LED_BOUCHE_2, GPIO.LOW) #On l'eteint 
            GPIO.output(PIN_LED_BOUCHE_3, GPIO.LOW) #On l'eteint 


class DragonHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        global isMotionActive, ENABLE_SOUND, forcePierreUntil, mixer
        params = urlparse(self.path)
        if(params.path == '/'):
            self.send_response(200)
            self.send_header("Content-Type", "text/html; charset=utf-8")
            self.end_headers()
            with open("www/dragon.html") as f: 
                self.wfile.write(f.read().encode())
        else:
            self.send_response(200)
            self.send_header("Content-Type", "application/json; charset=utf-8")
            if(params.path == '/api/enableMotion'):
                enableStatus()
                self._sendStatus()
            elif(params.path == '/api/disableMotion'):
                disableStatus()
                self._sendStatus()
            elif(params.path == '/api/enableSound'):
                ENABLE_SOUND = True
                self._sendStatus()
            elif(params.path == '/api/disableSound'):
                ENABLE_SOUND = False
                self._sendStatus()
            elif(params.path == '/api/enablePierre'):
                now = datetime.now()
                timestamp = datetime.timestamp(now)
                forcePierreUntil = timestamp + 15
                cyclePierre()
                self._sendStatus()
            elif(params.path == '/api/disablePierre'):
                now = datetime.now()
                timestamp = datetime.timestamp(now)
                forcePierreUntil = timestamp + 15
                cyclePierre()
                self._sendStatus()
            elif(params.path == '/api/enableBouche'):
                cycleFront()
                self._sendStatus()
            elif(params.path == '/api/disableBouche'):
                cycleFront()
                self._sendStatus()
            elif(params.path == '/api/son1'):
                if not mixer.get_busy():
                    sndDragon1.play()
                self._sendStatus()
            elif(params.path == '/api/son2'):
                if not mixer.get_busy():
                    sndDragon2.play()
                self._sendStatus()
            elif(params.path == '/api/son3'):
                if not mixer.get_busy():
                    sndDragon3.play()
                self._sendStatus()
            elif(params.path == '/api/checkStatus'):
                self._sendStatus()
            elif(params.path == '/api/poweroff'):
                print("poweroff")
                os.system('/usr/sbin/poweroff')

    def _sendStatus(self):
        global isMotionActive, ENABLE_SOUND, hasMotionFront, hasMotionPierre
        self.end_headers()
        data = {}
        data["motionOn"] = isMotionActive
        data["soundOn"] = ENABLE_SOUND
        data["pierreOn"] = hasMotionPierre
        data["boucheOn"] = hasMotionFront
        data["son1On"] = True
        data["son2On"] = True
        data["son3On"] = True
        jsonstr = json.dumps(data, indent=4)
        self.wfile.write(jsonstr.encode(encoding='utf_8'))


print("DragonPi (CTRL+C to exit)")
#time.sleep(2)
#print("Ready")

try:
    # GPIO.RISING
    GPIO.add_event_detect(PIN_MOTION_PIERRE, GPIO.BOTH, callback=MOTION_PIERRE)
    GPIO.add_event_detect(PIN_MOTION_FRONT, GPIO.BOTH, callback=MOTION_FRONT)
    GPIO.add_event_detect(21, GPIO.BOTH, callback=PUSHBUTTON)
    cycleFront()
    port = 80
    with socketserver.TCPServer(("", port), DragonHandler) as httpd:
        HTTPDServer = httpd
        print("My PID is "+str(os.getpid()))
        print("serving at port : "+ str(port))
        curThread = threading.current_thread()
        httpd.serve_forever()
        print("serve_forever started")
except KeyboardInterrupt:
  print(" Quit")
  GPIO.cleanup()

 